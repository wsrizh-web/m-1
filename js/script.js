

$(document).ready(function () {
    $('.page').removeClass('preload');

    $('.nav-toggle').click(function () {
        $('.nav-toggle,.nav-mobile').toggleClass('active');
        $('body').toggleClass('noscroll')
    });
    $('.tab__content .search-list:not(:first-child)').css({display: 'none'});
    $('.tab__item').click(function () {
        if ($(this).hasClass('active')) return;
        $('.tab__item').removeClass('active');
        $(this).addClass('active');
        let index = $(this).index();
        $(`.search-list:not(:nth-child(${index + 1}))`).animate({
            opacity: 0
        }, 400, function () {
            $(`.search-list:not(:nth-child(${index + 1}))`).css({display: 'none'});
            $('.search-list').eq(index).css({display: 'block', opacity: 0}).animate({
                opacity: 1
            }, 400);
        });
    });


    $('.search__favorite').click(function () {
        let favorites = $(this).closest('.search-list').find('.search__favorite:checked');
        if (favorites.length > 1) {
            let index = $(this).closest('.search__card').index();
            $(this).closest('.search-list').find(`.search__favorite`).prop("checked", false)
                .eq(index).prop("checked", true);
        }
    });

    let bookingNumbers = 1;
    let bookingDate = new Date();
    let bookingDateR = `${bookingDate.getFullYear()}-${bookingDate.getMonth().toString().padStart(2, 0)}-${bookingDate.getDate().toString().padStart(2, 0)}`;
    let probability = [25, 50, 75, 100];
    $('.booking-accumulate__date').val(bookingDateR);

    function animateBooking() {
        setTimeout(() => {
            bookingNumbers++;
            $('.booking-accumulate__date,.booking-accumulate__number,.booking-accumulate__cost,.booking-accumulate__probability').css({
                fontSize: '0px'
            })
            setTimeout(() => {
                let accumulateDate = document.querySelector('.booking-accumulate__date');
                if(accumulateDate) accumulateDate.stepUp();
                $('.booking-accumulate__number').val(bookingNumbers);
                $('.booking-accumulate__cost').html(`${Math.round(Math.random() * (5000 - 2000) + 2000)} &#x20BD;`);
                $('.booking-accumulate__probability').html(`${probability[Math.round(Math.random() * (3 - 0))]}%`);
                $('.booking-accumulate__date,.booking-accumulate__number,.booking-accumulate__cost,.booking-accumulate__probability').removeAttr('style');
                if (bookingNumbers === 4) {
                    bookingNumbers = 0;
                    $('.booking-accumulate__date').val(bookingDateR);
                }
                animateBooking();
            }, 250);
        }, 3000);
    }

    animateBooking();

    $('.passenger-form__add').click(function (e) {
        e.preventDefault();
        $('.passenger-form__list').append(`
         <div class="form form_multi-col passenger-form__fieldset">
            <label class="form__label col-2">
                <input type="text" class="input test-2-name" placeholder=" ">
                <span class="input__name">Имя</span>
            </label>
            <label class="form__label col-2">
                <input type="text" class="input test-2-last" placeholder=" ">
                <span class="input__name">Фамилия</span>
            </label>
            <label class="form__label col-2">
                <input type="number" class="input test-2-name" placeholder=" ">
                <span class="input__name">Номер документа</span>
            </label>
            <label class="form__label col-2 date__label">
                <input type="date" class="input">
                <span class="input__name">Дата рождения</span>
            </label>
            <div class="btn search__btn passenger-form__delete" onclick="removePassenger(this)"><i class="fas fa-user-minus"></i></div>
        </div>
    `)
    });
});

function removePassenger(el) {
    let fieldset=$(el).closest('.passenger-form__fieldset');
    if(fieldset.siblings('.passenger-form__fieldset').length) {
        fieldset.remove();
    }
}